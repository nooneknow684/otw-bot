import "https://deno.land/std@0.208.0/dotenv/load.ts";
import { Command } from "https://deno.land/x/cliffy@v1.0.0-rc.3/command/mod.ts";

import { GrammyError } from "https://deno.land/x/grammy@v1.19.2/mod.ts";
import { setWebhook, setCommand, unsetWebHook } from "./bot/index.ts";
import { MyApp, toTimeLocale } from "./util.ts";
//

const run_command = new Command()
  .description("Run otw server")
  .action(async () => {
    const my_app = new MyApp();
    const abort_signal = new AbortController();

    Deno.addSignalListener("SIGINT", async () => {
      abort_signal.abort();
      await unsetWebHook(my_app);
      Deno.exit();
    });

    try {
      await setWebhook(my_app);
      await setCommand(my_app);
      const app = await my_app.app();
      await app.listen({
        hostname: my_app.config.host,
        port: parseInt(my_app.config.port),
        signal: abort_signal.signal,
      });
    } catch (e) {
      if (e instanceof GrammyError) {
        my_app.logger.error(e.description);
      } else {
        my_app.logger.error("Error when starting app:", e);
      }
    }
  });
const khanza_map_dotor = new Command()
  .description("Map doctor from Khanza Server")
  .action(async () => {
    const my_app = new MyApp(true);
    const service = await my_app.db();
    const khanza = my_app.khanza();

    await service.addDoctors(await khanza.getDoctors());
    Deno.exit(0);
  });

const khanza_notify_patient = new Command()
  .description("Notify patients about doctor otw status")
  .action(async () => {
    const my_app = new MyApp(true);
    const service = await my_app.db();
    const khanza = my_app.khanza();
    const sender = my_app.sender();
    const logger = my_app.logger;

    const now = new Date();

    const doctor_notify = await service.getDoctorNotify(now);

    doctor_notify.forEach((element) => {
      logger.info(`Getting patient for doctor : [${element.doctor_code}] `);
    });

    const [patientOfDoctorWillOtw, patientOfDoctorAllmostArrive] =
      await Promise.all([
        khanza.getPatientOfDoctors(
          now,
          doctor_notify.filter((e) => e.notify_otw).map((e) => e.doctor_code),
        ),
        khanza.getPatientOfDoctors(
          now,
          doctor_notify
            .filter((e) => e.notify_arrive)
            .map((e) => e.doctor_code),
        ),
      ]);

    const grouped = doctor_notify
      .map((doctor) => {
        return [
          ...patientOfDoctorWillOtw
            .filter((e) => e.doctor_code == doctor.doctor_code)
            .map((e) => ({
              phone: e.patient_phone,
              message: `Halo ${e.patient_name}, ${
                e.doctor_name
              } akan berangkat sekiter jam ${toTimeLocale(
                doctor.otw_at,
              )}. kemungkinan sampai di tempat jam ${toTimeLocale(
                doctor.eta_at,
              )}`,
            })),
          ...patientOfDoctorAllmostArrive
            .filter((e) => e.doctor_code == doctor.doctor_code)
            .map((e) => ({
              phone: e.patient_phone,
              message: `Halo ${e.patient_name}, ${e.doctor_name} sudah mau sampai nih`,
            })),
        ];
      })
      .flat();

    if (grouped.length > 0) {
      await sender.sendMessages(grouped);
    }

    Deno.exit(0);
  });

await new Command()
  .description("Aplikasi otw bot")
  .version("1.0.0")
  .command("run", run_command)
  .command("khanza:doctor", khanza_map_dotor)
  .command("khanza:notify-patient", khanza_notify_patient)
  .parse(Deno.args);
