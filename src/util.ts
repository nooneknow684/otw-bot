import Logger from "https://deno.land/x/logger@v1.1.3/logger.ts";
import { Bot } from "https://deno.land/x/grammy@v1.19.2/mod.ts";

import { DbService, initService } from "./db/index.ts";
import { AppConfig, getConfig } from "./config.ts";
import { initBot } from "./bot/index.ts";
import { MyContext } from "./bot/middleware.ts";
import { Server, initServer } from "./server/index.ts";
import { initKhanzaService, KhanzaService } from "./khanza/index.ts";
import { Sender, initSender } from "./sender/index.ts";

export function stringifyEta(eta: number) {
  const hours = Math.floor(eta / 60);
  const minutes = eta % 60;

  const result_string = minutes > 0 ? ` ${minutes} menit` : "";

  return hours > 0 ? `${hours} jam${result_string}` : result_string;
}

export function toTimeLocale(date: Date) {
  return date.toLocaleTimeString("id", {
    hour: "2-digit",
    minute: "2-digit",
    timeZoneName: "short",
    hour12: false,
  });
}

export class MyApp {
  private _logger: Logger;
  private _service?: DbService;
  private _config: AppConfig;
  private _bot?: Bot<MyContext>;
  private _app?: Server;
  private _khanza?: KhanzaService;
  private _sender?: Sender;

  constructor(show_log = false) {
    this._logger = new Logger();
    this._config = getConfig({ logger: this._logger });
  }

  get logger() {
    return this._logger;
  }

  get config() {
    return this._config;
  }

  sender() {
    if (!this._sender) {
      this._sender = initSender(this);
    }
    return this._sender;
  }

  khanza() {
    if (!this._khanza) {
      this._khanza = initKhanzaService(this);
    }
    return this._khanza;
  }

  async db() {
    if (!this._service) {
      this._service = await initService(this);
    }
    return this._service;
  }

  async bot() {
    if (!this._bot) {
      this._bot = await initBot(this);
    }
    return this._bot;
  }

  async app() {
    if (!this._app) {
      this._app = await initServer(this);
    }
    return this._app;
  }
}
