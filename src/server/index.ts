import { MyApp } from "../util.ts";
import { Router, Application } from "https://deno.land/x/oak@v12.6.1/mod.ts";
import { webhookCallback } from "https://deno.land/x/grammy@v1.19.2/mod.ts";

export async function initServer(app: MyApp) {
  const bot = await app.bot();

  const router = new Router();
  router
    .get(
      "/checkhealth",
      (ctx) =>
        (ctx.response.body = {
          status: "OK",
        }),
    )
    .post("/hook", webhookCallback(bot, "oak"));

  const my_app = new Application();

  my_app.addEventListener("error", (evt) => {
    app.logger.error(evt.error);
  });

  my_app.addEventListener("listen", ({ hostname, port, secure }) => {
    app.logger.info(`Start listening to ${hostname}:${port} [${secure}]`);
  });

  my_app.use(router.routes());
  my_app.use(router.allowedMethods());
  return my_app;
}

export type Server = Awaited<ReturnType<typeof initServer>>;
