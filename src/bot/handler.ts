import { NextFunction } from "https://deno.land/x/grammy@v1.19.2/mod.ts";
import { MyContext } from "./middleware.ts";
import { DbService } from "../db/index.ts";

export async function showHelpHanlder(ctx: MyContext) {
  await ctx.reply(
    "Hello, I am a bot. I can help you with your query. Please, write your query and I will try to help you. ",
  );
}

export async function exitHandler(ctx: MyContext) {
  await ctx.conversation.exit();
}

export async function registerHandler(ctx: MyContext) {
  await ctx.conversation.enter("register");
}

export async function redirectToPrivateChatHandler(ctx: MyContext) {
  await ctx.reply(
    `Kalo mau kenalan, mohon dilakukan di private chat ya. silahkan gunakan tombol berikut`,
    {
      reply_markup: {
        inline_keyboard: [
          [{ text: "Kenalan", url: "tg://resolve?domain=hp2k_bot" }],
        ],
      },
    },
  );
}

export async function otwHandler(ctx: MyContext) {
  await ctx.conversation.enter("otw");
}

export function buildReaskOtwHandler(service: DbService) {
  return async function (ctx: MyContext, next: NextFunction) {
    if (
      await service.isDoctorHasOtw(
        ctx.otw_user!.doctor!.code,
        new Date(new Date().setHours(0, 0, 0)),
      )
    )
      return await ctx.conversation.enter("reask_otw");
    await next();
  };
}
