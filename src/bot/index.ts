import { Bot, session } from "https://deno.land/x/grammy@v1.19.2/mod.ts";
import {
  conversations,
  createConversation,
} from "https://deno.land/x/grammy_conversations@v1.1.2/mod.ts";
import {
  MyContext,
  buildCheckUserMiddlerware,
  doctorMiddleware,
  notRegisteredMiddleware,
} from "./middleware.ts";
import {
  buildOtwConversation,
  buildReaskOtwConversation,
  buildRegisterConversation,
} from "./conversation/mod.ts";
import { hydrateReply } from "https://deno.land/x/grammy_parse_mode@1.8.1/mod.ts";
import {
  exitHandler,
  otwHandler,
  redirectToPrivateChatHandler,
  registerHandler,
  showHelpHanlder,
  buildReaskOtwHandler,
} from "./handler.ts";
import { MyApp } from "../util.ts";

export async function initBot(app: MyApp) {
  const bot = new Bot<MyContext>(app.config.bot_token);
  const service = await app.db();

  bot.use(
    session({
      initial: () => ({}),
    }),
  );

  bot.use(conversations());
  bot.use(hydrateReply);

  // Generic handler
  bot.on("message").command("help", showHelpHanlder);
  bot.on("message").command("exit", exitHandler);

  bot.use(buildCheckUserMiddlerware(service));

  bot.on("message").command("lupakan", async (ctx) => {
    if (ctx.otw_user?.doctor) {
      await service.removeDoctorIdentity(ctx.otw_user.doctor.code);
      await ctx.reply(
        "Aku lupa akan sesuatu.\nEeh, mungkin bukan sesuati yang penting",
      );
      ctx.otw_user = undefined;
    }
  });

  const mours = {
    service: await app.db(),
    logger: app.logger,
  };

  bot.use(createConversation(buildRegisterConversation(mours), "register"));
  bot.use(createConversation(buildOtwConversation(mours), "otw"));
  bot.use(createConversation(buildReaskOtwConversation(mours), "reask_otw"));

  const message_only = bot.on("message");

  message_only
    .command("kenalan")
    .use(notRegisteredMiddleware)
    .branch(
      (ctx) => ctx.chat.type == "private",
      registerHandler,
      redirectToPrivateChatHandler,
    );

  message_only
    .chatType("private")
    .command("otw")
    .use(doctorMiddleware, buildReaskOtwHandler(service), otwHandler);

  return bot;
}

export async function setCommand(app: MyApp) {
  const bot = await app.bot();
  await bot.api.setMyCommands([
    // { command: "start", description: "Start the bot" },
    { command: "help", description: "Show help text" },
    { command: "kenalan", description: "Kenalan dulu dengan bot G-Care" },
    { command: "otw", description: "[Dokter] Atur Jadwal OTW" },
    { command: "lupakan", description: "Lupakan aku" },
  ]);
}

export async function setWebhook(app: MyApp) {
  const { config, logger } = app;
  const bot = await app.bot();

  logger.info("Setting telegram webhook");

  await bot.api.setWebhook(
    `${config.domain_protocol}://${config.domain_name}/hook`,
  );

  logger.info("Setting telegram web hook success");
}

export async function unsetWebHook(app: MyApp) {
  const { logger } = app;
  const bot = await app.bot();

  logger.info("Unsetting telegram webhook");

  await bot.api.setWebhook("");

  logger.info("Unsetting telegram web hook success");
}
