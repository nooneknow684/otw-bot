import {
  Context,
  NextFunction,
  SessionFlavor,
} from "https://deno.land/x/grammy@v1.19.2/mod.ts";
import { ConversationFlavor } from "https://deno.land/x/grammy_conversations@v1.1.2/conversation.ts";
import { Doctor } from "../db/model.ts";
import { DbService } from "../db/index.ts";
import {
  ParseModeFlavor,
  fmt,
  link,
} from "https://deno.land/x/grammy_parse_mode@1.8.1/mod.ts";

type OtwContext = {
  otw_user?: { doctor?: Doctor; employee?: null };
};

export type MyContext = ParseModeFlavor<
  ConversationFlavor &
    SessionFlavor<Record<string, null>> &
    Context &
    OtwContext
>;

export function buildCheckUserMiddlerware(service: DbService) {
  return async function (ctx: MyContext, next: NextFunction) {
    let user;

    const { from } = ctx;

    if (from) {
      user = await service.getUserIdRelatedUser(from.id);
    }

    if (user) {
      ctx.otw_user = {
        ...user,
      };
    }

    await next();
  };
}

export async function registeredMiddleware(ctx: MyContext, next: NextFunction) {
  if (ctx.otw_user) return await next();
  await ctx.reply(
    `Eh kita belum kenalan lho, kenalan dulu yu di private chat. silahkan gunakan tombol berikut`,
    {
      reply_markup: {
        inline_keyboard: [
          [{ text: "Kenalan", url: "tg://resolve?domain=hp2k_bot" }],
        ],
      },
    },
  );
}

export async function doctorMiddleware(ctx: MyContext, next: NextFunction) {
  if (ctx.otw_user?.doctor) return await next();
  await ctx.reply(`Maaf ya, cuman doktor yang bisa mengakses fitur ini`);
}
export async function notRegisteredMiddleware(
  ctx: MyContext,
  next: NextFunction,
) {
  if (ctx.otw_user?.doctor) {
    const {
      doctor: { name, user_id },
    } = ctx.otw_user;

    await ctx.replyFmt(
      fmt`Halo ${fmt`${link(
        name,
        `tg://user?id=${user_id}`,
      )}`}, kita udah kenalan ya, ada yang bisa saya bantu?`,
    );

    return;
  }
  await next();
}
