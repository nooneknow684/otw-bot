import { Conversation } from "https://deno.land/x/grammy_conversations@v1.1.2/conversation.ts";
import { DbService } from "../../db/index.ts";
import { Logger } from "https://deno.land/x/logger@v1.1.3/mod.ts";
import { MyContext } from "../middleware.ts";

export type MyConversation = Conversation<MyContext>;
export type ConversationBuilderParam = {
  service: DbService;
  logger: Logger;
};

export { buildRegisterConversation } from "./register.ts";
export { buildReaskOtwConversation } from "./reask_otw.ts";
export { buildOtwConversation } from "./ask_otw.ts";
