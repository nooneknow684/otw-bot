import { ConversationBuilderParam, MyConversation } from "./mod.ts";
import { MyContext } from "../middleware.ts";
import { stringifyEta, toTimeLocale } from "../../util.ts";
import { Message } from "https://deno.land/x/grammy_types@v3.3.0/mod.ts";

export function buildOtwConversation(app: ConversationBuilderParam) {
  const { service, logger } = app;
  return async function (conv: MyConversation, ctx: MyContext) {
    while (true) {
      const otw_time = await askOtwAt(conv, ctx);

      const eta = await askEtaAt(conv, ctx);
      const eta_time = new Date(otw_time.valueOf() + eta * 60 * 1000);

      const msg = `Ok dok, dikonfirmasi dokter berangkat jam ${toTimeLocale(
        otw_time,
      )}, dan tiba dalam ${stringifyEta(eta)} atau pukul ${toTimeLocale(
        eta_time,
      )} ya dok?`;

      const result = await askConfirmation(conv, ctx, msg);

      if (result) {
        await conv.external(
          async () =>
            await service.addDoctorOtw(
              ctx.otw_user!.doctor!.code,
              new Date(new Date(otw_time).setHours(0, 0, 0)),
              { otw_at: otw_time, eta_at: eta_time },
            ),
        );
        logger.info(
          `Assign Otw : [${ctx.otw_user!.doctor!.code}] ${
            ctx.otw_user!.doctor!.name
          } at ${otw_time.toLocaleTimeString()} arrive at ${eta_time.toLocaleTimeString()}`,
        );
        await ctx.api.editMessageText(
          result.chat.id,
          result.message_id,
          `OK dok, pasien akan dikabarkan waktu kedatangan dokter. OTW jam ${toTimeLocale(
            otw_time,
          )} dengan estimasi waktu kedatangan ${toTimeLocale(
            eta_time,
          )} (${stringifyEta(eta)})`,
        );
        return;
      }
    }
  };

  async function askOtwAt(conv: MyConversation, ctx: MyContext) {
    let otw = new Date(await conv.now());
    const reply = await ctx.reply("Mau otw kapan dok?", {
      reply_markup: {
        inline_keyboard: [
          [
            { callback_data: "ASK_OTW:NOW", text: "Sekarang" },
            { callback_data: "ASK_OTW:LATER", text: "Nanti" },
          ],
        ],
      },
    });

    const otw_when_answer = await conv.waitFor("callback_query:data");

    switch (otw_when_answer.callbackQuery.data) {
      case "ASK_OTW:LATER": {
        await ctx.api.editMessageText(
          reply.chat.id,
          reply.message_id,
          "Ok dok, mau otw jam berapa dok?\nContoh pengisian 13.30 / 13:30",
        );
        otw = await askOtwLater(conv, ctx, otw);
        break;
      }
      default:
        break;
    }

    await ctx.api.editMessageText(
      reply.chat.id,
      reply.message_id,
      `Ok dok, Dokter OTW jam ${toTimeLocale(otw)} ya dok`,
    );

    return otw;
  }

  async function askOtwLater(conv: MyConversation, ctx: MyContext, now: Date) {
    const pattern = /(\d\d)[:.,](\d\d)/;
    let error_message: Message.TextMessage | null = null;

    let result;

    while (true) {
      const answer = await conv.waitFor("msg:text");

      const match = answer.msg.text.match(pattern);
      if (match) {
        const hour = parseInt(match[1]);
        const minute = parseInt(match[2]);

        if (
          !isNaN(hour) &&
          !isNaN(minute) &&
          (hour < 24 || hour >= 0) &&
          (minute < 60 || hour >= 0)
        ) {
          result = {
            hour,
            minute,
          };
          break;
        }
      }

      await answer.deleteMessage();

      if (!error_message) {
        error_message = await ctx.api.sendMessage(
          answer.chat.id,
          "Maaf, format jamnya salah ya dok.\nContoh pengisian 13.30 / 13:30",
        );
      }
    }

    if (error_message) {
      await ctx.api.deleteMessage(
        error_message.chat.id,
        error_message.message_id,
      );
    }

    return new Date(now.setHours(result.hour, result.minute, 0, 0));
  }

  async function askEtaAt(conv: MyConversation, ctx: MyContext) {
    const reply = await ctx.reply("Kemungkinan tiba berapa menit ya dok?");

    let eta;
    let error_message;

    while (true) {
      const result = await conv.waitFor("msg:text");
      eta = parseInt(result.msg.text);

      if (!isNaN(eta)) {
        break;
      }

      await result.deleteMessage();

      if (!error_message) {
        error_message = await ctx.reply(
          "Maaf dok, formatnya salah ya dok, mohon kirim angka saja",
        );
      }
    }
    if (error_message) {
      await ctx.api.deleteMessage(
        error_message.chat.id,
        error_message.message_id,
      );
    }
    await ctx.api.editMessageText(
      reply.chat.id,
      reply.message_id,
      `Ok dok, waktu tiba sekitar ${eta} menit ya dok`,
    );
    return eta;
  }

  async function askConfirmation(
    conv: MyConversation,
    ctx: MyContext,
    msg: string,
  ) {
    const confirm_reply = await ctx.reply(msg, {
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: "Ya",
              callback_data: "OTW:CONFIRM:OK",
            },
            {
              text: "Atur Ulang",
              callback_data: "OTW:CONFIRM:NO",
            },
          ],
        ],
      },
    });

    const confirm = await conv.waitFor("callback_query:data");
    await confirm.answerCallbackQuery();

    if (confirm.callbackQuery.data == "OTW:CONFIRM:OK") return confirm_reply;
  }
}
