import { ConversationBuilderParam, MyConversation } from "./mod.ts";
import { MyContext } from "../middleware.ts";
import { buildOtwConversation } from "./ask_otw.ts";
export function buildReaskOtwConversation(app: ConversationBuilderParam) {
  const { service } = app;
  return async function (conv: MyConversation, ctx: MyContext) {
    const now = new Date(await conv.now());
    const start_of_day = new Date(now.setHours(0, 0, 0));

    const otw = await conv.external(async () => {
      return await service.getDoctorOtw(
        ctx.otw_user!.doctor!.code,
        start_of_day,
      );
    });

    if (!otw) return;

    const reply = await ctx.reply(
      `Dokter sudah mengisi jadwal otw ya, dokter berangkat jam ${otw.otw_at.toLocaleTimeString()} dan tiba sekitar jam ${otw.eta_at.toLocaleTimeString()},\nMau diubah dok?`,
      {
        reply_markup: {
          inline_keyboard: [
            [
              {
                text: "Ubah",
                callback_data: "REASK:CONFIRM:CHANGE",
              },
              {
                text: "Batal Otw",
                callback_data: "REASK:CONFIRM:CANCEL",
              },
              {
                text: "Tidak",
                callback_data: "REASK:CONFIRM:EXIT",
              },
            ],
          ],
        },
      },
    );

    const result = await conv.waitFor("callback_query:data");
    await result.answerCallbackQuery();

    switch (result.callbackQuery.data) {
      case "REASK:CONFIRM:CHANGE":
        await ctx.api.deleteMessage(reply.chat.id, reply.message_id);
        await buildOtwConversation(app)(conv, ctx);
        break;
      case "REASK:CONFIRM:CANCEL":
        await conv.external(async () => {
          await service.cancelDoctorOtw(
            ctx.otw_user!.doctor!.code,
            start_of_day,
          );
        });
        await ctx.api.editMessageText(
          reply.chat.id,
          reply.message_id,
          "Jadwal otw nya di batalkan ya dok",
        );
        return;
      case "REASK:CONFIRM:EXIT":
        await ctx.api.editMessageText(
          reply.chat.id,
          reply.message_id,
          "Ok, dok",
        );
        return;
    }
  };
}
