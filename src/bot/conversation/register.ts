import { Message } from "https://deno.land/x/grammy_types@v3.3.0/message.ts";
import { ConversationBuilderParam, MyConversation } from "./mod.ts";
import { MyContext } from "../middleware.ts";
const DOCTOR_PER_PAGE = 10;

export function buildRegisterConversation(app: ConversationBuilderParam) {
  return async function (conv: MyConversation, ctx: MyContext) {
    const reply = await ctx.reply(
      "Haloo, kenalan dulu kita, kalo boleh tau posisi anda apa ya?",
      {
        reply_markup: {
          inline_keyboard: [
            [
              { text: "Dokter", callback_data: "REGISTER:DOCTOR" },
              { text: "Pegawai", callback_data: "REGISTER:EMPLOYEE" },
            ],
          ],
        },
      },
    );

    const position = await conv.waitFor("callback_query:data");
    await position.answerCallbackQuery();
    switch (position.callbackQuery.data) {
      case "REGISTER:DOCTOR":
        await handleDoctorRegistration(app, reply, conv, ctx);
        break;
      case "REGISTER:EMPLOYEE":
        await ctx.api.editMessageText(
          reply.chat.id,
          reply.message_id,
          "Maaf, fitur ini belum tersedia ya bos ku",
        );
        break;
    }

    return;
  };
}

async function handleDoctorRegistration(
  app: ConversationBuilderParam,
  message: Message.TextMessage,
  conv: MyConversation,
  ctx: MyContext,
) {
  const { service, logger } = app;

  let name = "";
  let page = 1;

  while (true) {
    const doctors_keyboard_markup = (
      await conv.external(
        async () => await service.getDoctors(name, page, DOCTOR_PER_PAGE),
      )
    ).map((doctor) => [
      {
        text: doctor.name,
        callback_data: `REGISTER:CODE:${doctor.code}`,
      },
    ]);

    await ctx.api.editMessageText(
      message.chat.id,
      message.message_id,
      "Silahkan pilih identitas anda ya dok",
      {
        reply_markup: {
          inline_keyboard: [
            ...doctors_keyboard_markup,
            ...[
              [
                {
                  text: "<<",
                  callback_data: "REGISTER:PREV",
                },
                {
                  text: "Batal",
                  callback_data: "REGISTER:CANCEL",
                },
                {
                  text: ">>",
                  callback_data: "REGISTER:NEXT",
                },
              ].slice(
                page == 1 ? 1 : undefined,
                doctors_keyboard_markup.length < DOCTOR_PER_PAGE
                  ? -1
                  : undefined,
              ),
              [
                {
                  text: "Hapus Filter",
                  callback_data: "REGISTER:CLEAR",
                },
              ],
            ].slice(0, name == "" ? -1 : undefined),
          ],
        },
      },
    );

    const response = await conv.waitFor([
      "callback_query:data",
      "message:text",
    ]);

    if (response.message) {
      await response.deleteMessage();
      name = response.message.text;
      continue;
    }

    if (response.callbackQuery) {
      await response.answerCallbackQuery();
      const data = response.callbackQuery.data;

      if (data.startsWith("REGISTER:CODE:")) {
        const doctor_code = data.slice("REGISTER:CODE:".length);
        const { chat, from } = response;
        if (!chat) return;

        const doctor = await conv.external(async () => {
          return await service.getDoctorByCode(doctor_code).then((doctor) => {
            if (!doctor) return;
            return service
              .updateDoctorIdentity(doctor.code, chat.id, from.id)
              .then(() => doctor);
          });
        });

        if (doctor) {
          logger.info(
            `Doctor registration [${doctor.code}] ${doctor.name} ${doctor.speciality_code} ${doctor.speciality}`,
          );
          await ctx.api.editMessageText(
            message.chat.id,
            message.message_id,
            `Halo ${doctor.name}, salam kenal.`,
          );
          return;
        }
      }

      switch (response.callbackQuery.data) {
        case "REGISTER:PREV":
          page--;
          continue;
        case "REGISTER:NEXT":
          page++;
          continue;
        case "REGISTER:CLEAR":
          name = "";
          page = 1;
          continue;
        case "REGISTER:CANCEL":
          await ctx.api.editMessageText(
            message.chat.id,
            message.message_id,
            "Yah, gak jadi kenalan dong",
          );
          return;
      }
    }

    break;
  }
}
