import { MyApp } from "../util.ts";

export type NotifyMessage = {
  phone: string;
  message: string;
};

export function initSender(app: MyApp) {
  return {
    async sendMessages(messages: NotifyMessage[]) {
      const payload = {
        data: messages,
      };

      messages.forEach(({ phone, message }) => {
        app.logger.info(`Sending message to ${phone} : "${message}"`);
      });

      const request = new Request(
        `${app.config.wablas_domain}/api/v2/send-message`,
        {
          method: "POST",
          body: JSON.stringify(payload),
          headers: {
            "Content-Type": "application/json",
            Authorization: app.config.wablas_token,
          },
        },
      );

      const response = await fetch(request);
      if (response.status != 200) {
        throw new Error(await response.json());
      }
    },
    async sendMessage(message: NotifyMessage) {
      await this.sendMessages([message]);
    },
  };
}

export type Sender = Awaited<ReturnType<typeof initSender>>;
