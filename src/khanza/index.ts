import { Kysely, MysqlDialect, MysqlPool, sql } from "kysely";
import { MyApp } from "../util.ts";
import { createPool } from "mysql2";
import { Database } from "./model.ts";

export function initKhanzaService(app: MyApp) {
  app.logger.info("Connecting to khanza server");

  const dialect = new MysqlDialect({
    pool: createPool({
      uri: app.config.khanza_url,
      connectionLimit: 10,
    }) as unknown as MysqlPool,
  });

  app.logger.info("Connection to khanza server success");

  const db = new Kysely<Database>({ dialect });

  return {
    async getDoctors() {
      return await db
        .selectFrom("dokter")
        .leftJoin("spesialis", "dokter.kd_sps", "spesialis.kd_sps")
        .select([
          "dokter.kd_dokter as code",
          "dokter.nm_dokter as name",
          "spesialis.kd_sps as speciality_code",
          "spesialis.nm_sps as speciality",
        ])
        .execute();
    },
    async getPatientOfDoctors(date: Date, doctors: string[]) {
      if (doctors.length == 0) return [];

      const start_of_day = new Date(date.setHours(0, 0, 0, 0));
      return await db
        .with("limited_pasien", (inner) => {
          return inner
            .selectFrom("reg_periksa")
            .innerJoin(
              "pasien",
              "pasien.no_rkm_medis",
              "reg_periksa.no_rkm_medis",
            )
            .innerJoin("dokter", "dokter.kd_dokter", "reg_periksa.kd_dokter")
            .where("reg_periksa.tgl_registrasi", "=", start_of_day)
            .where("reg_periksa.kd_dokter", "in", doctors)
            .select((cb) => [
              "pasien.nm_pasien as patient_name",
              "pasien.no_tlp as patient_phone",
              "dokter.nm_dokter as doctor_name",
              "dokter.kd_dokter as doctor_code",
              sql`ROW_NUMBER() OVER ( PARTITION BY ${cb.ref(
                "dokter.kd_dokter",
              )} ORDER BY ${cb.ref("tgl_registrasi")} ASC)`.as("row_number"),
            ]);
        })
        .selectFrom("limited_pasien")
        .select(["patient_name", "patient_phone", "doctor_name","doctor_code"])
        .where("row_number", "<=", "5")
        .execute();
    },
  };
}

export type KhanzaService = ReturnType<typeof initKhanzaService>;
