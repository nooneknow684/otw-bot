interface DoctorTable {
  kd_dokter: string;
  nm_dokter: string;
  kd_sps: string;
}

interface RegistrationTable {
  no_rawat: string;
  no_rkm_medis: string;
  kd_dokter:string,
  tgl_registrasi: Date;
}

interface PatientTable {
  no_rkm_medis: string;
  nm_pasien: string;
  no_tlp: string;
}

interface SpecialityTable {
  kd_sps: string;
  nm_sps: string;
}

export interface Database {
  dokter: DoctorTable;
  spesialis: SpecialityTable;
  reg_periksa: RegistrationTable;
  pasien: PatientTable;
}
