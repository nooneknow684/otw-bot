import { Logger } from "https://deno.land/x/logger@v1.1.3/mod.ts";

export function getConfig({ logger }: { logger: Logger }) {
  logger.info("Getting configration");

  return {
    database_url: Deno.env.get("DATABASE_URL")!,
    khanza_url: Deno.env.get("KHANZA_URL")!,

    host: Deno.env.get("HOST")!,
    port: Deno.env.get("PORT")!,

    domain_name: Deno.env.get("DOMAIN_NAME")!,
    domain_protocol: Deno.env.get("DOMAIN_PROTOCOL")!,

    bot_token: Deno.env.get("BOT_TOKEN")!,

    wablas_token: Deno.env.get("WABLAS_TOKEN")!,
    wablas_domain: Deno.env.get("WABLAS_DOMAIN")!,
  };
}

export type AppConfig = ReturnType<typeof getConfig>;
