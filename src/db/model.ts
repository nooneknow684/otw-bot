import { ColumnType, Generated, Selectable, Updateable } from "kysely";

interface DoctorTable {
  code: string;
  name: string;
  speciality: ColumnType<string, string | null | undefined, string>;
  speciality_code: ColumnType<string, string | null | undefined, string>;
  chat_id: ColumnType<number | null, number | null, number | null>;
  user_id: ColumnType<number | null, number | null, number | null>;
}

interface OtwTable {
  id: Generated<number>;
  doctor_code: string;
  otw_date: ColumnType<Date, Date, Date>;
  otw_at: ColumnType<Date, Date, Date>;
  eta_at: ColumnType<Date, Date, Date>;
  arrive_at: ColumnType<
    Date | null,
    Date | null | undefined,
    Date | null | undefined
  >;
  notify_arrive_at: ColumnType<
    Date | null,
    Date | null | undefined,
    Date | null | undefined
  >;
  created_at: Generated<Date>;
  updated_at: Generated<Date>;
}

export type DoctorCreate = {
  code: string;
  name: string;
  speciality: string | null | undefined;
  speciality_code: string | null | undefined;
};
export type DoctorUpdate = Partial<Updateable<DoctorTable>>;
export type OtwCreate = {
  otw_at: Date;
  eta_at: Date;
};

export type OtwUpdate = {
  otw_at: Date;
  eta_at: Date;
};

export type Doctor = Selectable<DoctorTable>;

export interface Database {
  doctors: DoctorTable;
  otws: OtwTable;
}
