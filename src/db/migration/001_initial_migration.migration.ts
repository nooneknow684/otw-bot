import { Kysely, sql } from "kysely";

export async function up(db: Kysely<any>) {
  await db.schema
    .createTable("doctors")
    .addColumn("code", "varchar(20)")
    .addColumn("name", "varchar(20)", (col) => col.notNull())
    .addColumn("speciality", "varchar(20)", (col) => col.notNull())
    .addColumn("speciality_code", "varchar(20)", (col) => col.notNull())
    .addColumn("chat_id", "bigint")
    .addColumn("user_id", "bigint")
    .addPrimaryKeyConstraint("pk_doctors", ["code"])
    .execute();

  await db.schema
    .createTable("otws")
    .addColumn("id", "bigint", (col) => col.autoIncrement())
    .addColumn("doctor_code", "varchar(20)", (col) => col.notNull())
    .addColumn("otw_date", "date", (col) => col.notNull())
    .addColumn("otw_at", "datetime", (col) => col.notNull())
    .addColumn("eta_at", "datetime", (col) => col.notNull())
    .addColumn("arrive_at", "datetime")
    .addColumn("notify_patient_at", "datetime")
    .addColumn("updated_at", "datetime", (col) =>
      col
        .defaultTo(sql`current_timestamp`)
        .modifyEnd(sql`on update current_timestamp`),
    )
    .addColumn("created_at", "datetime", (col) =>
      col.defaultTo(sql`current_timestamp`),
    )
    .addPrimaryKeyConstraint("pk_otws", ["id"])
    .addUniqueConstraint("uq_otws_doctor_code_otw_date", [
      "doctor_code",
      "otw_date",
    ])
    .addForeignKeyConstraint(
      "fk_otws_doctors",
      ["doctor_code"],
      "doctors",
      ["code"],
      (fk) => fk.onDelete("cascade").onUpdate("cascade"),
    )
    .execute();
}

export async function down(db: Kysely<any>) {
  await db.schema.dropTable("otws").execute();
  await db.schema.dropTable("doctors").execute();
}
