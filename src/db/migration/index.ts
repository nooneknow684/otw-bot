import { MigrationProvider, Migration } from "kysely";
export class MyMigrationProvider implements MigrationProvider {
  async getMigrations(): Promise<Record<string, Migration>> {
    const migrations: Record<string, Migration> = {};

    const files = [...Deno.readDirSync("./src/db/migration")]
      .filter((f) => f.isFile && f.name.endsWith("migration.ts"))
      .sort((a, b) => a.name.localeCompare(b.name));
    for (const file of files) {
      const migration = await import(`./${file.name}`);
      const migrationKey = file.name.substring(0, file.name.lastIndexOf("."));
      migrations[migrationKey] = migration;
    }

    return migrations;
  }
}
