import { MysqlDialect, Migrator, MysqlPool } from "kysely";
import { Kysely } from "kysely";
import { createPool } from "mysql2";

import { MyMigrationProvider } from "./migration/index.ts";
import {
  Database,
  Doctor,
  DoctorCreate,
  OtwCreate,
  OtwUpdate,
} from "./model.ts";
import { MyApp } from "../util.ts";

export type DbService = Awaited<ReturnType<typeof initService>>;

export async function initService(app: MyApp) {
  const dialect = new MysqlDialect({
    pool: createPool({
      connectionLimit: 100,
      uri: app.config.database_url,
    }) as unknown as MysqlPool,
  });

  const db = new Kysely<Database>({ dialect });

  app.logger.info("Running pending migration");
  const migrator = new Migrator({
    db,
    provider: new MyMigrationProvider(),
  });
  const result = await migrator.migrateToLatest();
  if (result.error) {
    app.logger.error(result.error);
    throw new Error(`Error when running migration : ${result.error}`);
  }

  app.logger.info("Migration success");

  return {
    async getUserIdRelatedUser(
      user_id: number,
    ): Promise<{ doctor?: Doctor; employee?: null } | undefined> {
      const doctor = await this.getDoctorByUserId(user_id);
      if (doctor)
        return {
          doctor,
          employee: null,
        };
      return;
    },
    async getChatIdRelatedUser(
      chat_id: number,
    ): Promise<{ doctor?: Doctor; employee: null } | undefined> {
      const doctor = await this.getDoctorByChatId(chat_id);
      if (doctor)
        return {
          doctor: doctor,
          employee: null,
        };
      return;
    },
    async getDoctors(name: string, page = 1, per_page = 10): Promise<Doctor[]> {
      return await db
        .selectFrom("doctors")
        .selectAll()
        .where("name", "like", `%${name}%`)
        .limit(per_page)
        .offset((page - 1) * per_page)
        .execute();
    },
    async getDoctorByUserId(user_id: number): Promise<Doctor | undefined> {
      return await db
        .selectFrom("doctors")
        .selectAll()
        .where("user_id", "=", user_id)
        .executeTakeFirst();
    },
    async getDoctorByChatId(chat_id: number): Promise<Doctor | undefined> {
      return await db
        .selectFrom("doctors")
        .selectAll()
        .where("chat_id", "=", chat_id)
        .executeTakeFirst();
    },
    async getDoctorByCode(code: string): Promise<Doctor | undefined> {
      return await db
        .selectFrom("doctors")
        .selectAll()
        .where("code", "=", code)
        .executeTakeFirst();
    },
    async addDoctors(doctors: DoctorCreate[]) {
      return await db
        .insertInto("doctors")
        .values(doctors)
        .onDuplicateKeyUpdate((cb) => ({
          name: cb.fn("values", ["name"]),
          speciality: cb.fn("values", ["speciality"]),
          speciality_code: cb.fn("values", ["speciality_code"]),
        }))
        .ignore()
        .executeTakeFirst();
    },
    async updateDoctorIdentity(
      doctor_code: string,
      chat_id: number,
      user_id: number,
    ) {
      await db
        .updateTable("doctors")
        .set({ user_id, chat_id })
        .where("code", "=", doctor_code)
        .execute();
    },
    async removeDoctorIdentity(doctor_code: string) {
      await db
        .updateTable("doctors")
        .set({ user_id: null, chat_id: null })
        .where("code", "=", doctor_code)
        .execute();
    },

    async addDoctorOtw(doctor_code: string, otw_date: Date, otw: OtwCreate) {
      const start_of_day_local = new Date(otw_date.setHours(0, 0, 0, 0));
      await db
        .insertInto("otws")
        .values({ doctor_code, otw_date: start_of_day_local, ...otw })
        .onDuplicateKeyUpdate(otw)
        .execute();
    },
    async isDoctorHasOtw(doctor_code: string, otw_date: Date) {
      // get start_date from otw_date;
      const start_date = new Date(otw_date.setHours(0, 0, 0, 0));
      //getcount only
      const result = await db
        .selectFrom("otws")
        .select((eb) => [eb.fn.count<number>("doctor_code").as("count")])
        .where("doctor_code", "=", doctor_code)
        .where("otw_date", "=", start_date)
        .executeTakeFirst();
      if (!result) return false;

      return result.count > 0;
    },
    async cancelDoctorOtw(doctor_code: string, otw_date: Date) {
      // get start_date from otw_date;
      const start_date = new Date(otw_date.setHours(0, 0, 0, 0));
      //getcount only
      await db
        .deleteFrom("otws")
        .where("doctor_code", "=", doctor_code)
        .where("otw_date", "=", start_date)
        .executeTakeFirst();
    },
    async updateDoctorOtw(doctor_code: string, otw_date: Date, otw: OtwUpdate) {
      const start_of_day_local = new Date(otw_date.setHours(0, 0, 0, 0));
      await db
        .updateTable("otws")
        .set(otw)
        .where("doctor_code", "=", doctor_code)
        .where("otw_date", "=", start_of_day_local)
        .execute();
    },
    async updateDoctorArrive(
      doctor_code: string,
      otw_date: Date,
      arrive_at: Date,
    ) {
      const start_of_day_local = new Date(otw_date.setHours(0, 0, 0, 0));
      await db
        .updateTable("otws")
        .set({ arrive_at })
        .where("doctor_code", "=", doctor_code)
        .where("otw_date", "=", start_of_day_local)
        .execute();
    },

    async getDoctorOtw(doctor_code: string, otw_date: Date) {
      const start_of_day_local = new Date(otw_date.setHours(0, 0, 0, 0));
      return await db
        .selectFrom("otws")
        .selectAll()
        .where("doctor_code", "=", doctor_code)
        .where("otw_date", "=", start_of_day_local)
        .executeTakeFirst();
    },
    async getDoctorNotify(now: Date) {
      const max = now.valueOf();
      const min = max - 5 * 60 * 1000;
      let query = db
        .selectFrom("otws")
        .select((cb) => [
          "id",
          "doctor_code",
          "otw_at",
          "eta_at",
          cb
            .and([
              cb.cmpr("otw_at", ">=", now),
              cb.cmpr("otws.updated_at", "<=", new Date(max)),
              cb.cmpr("otws.updated_at", ">=", new Date(min)),
            ])
            .as("notify_otw"),
          cb
            .and([
              cb.cmpr("otws.eta_at", "<=", new Date(max)),
              cb.cmpr("otws.eta_at", ">=", new Date(min)),
            ])
            .as("notify_arrive"),
        ])
        .where((cb) => {
          return cb.or([
            cb.and([
              cb.cmpr("otw_at", ">=", now),
              cb.cmpr("otws.updated_at", "<=", new Date(max)),
              cb.cmpr("otws.updated_at", ">=", new Date(min)),
            ]),
            cb.and([
              cb.cmpr("otws.eta_at", "<=", new Date(max)),
              cb.cmpr("otws.eta_at", ">=", new Date(min)),
            ]),
          ]);
        });

      return await query.execute();
    },
  };
}
