FROM denoland/deno:1.38.4

ENV PORT=8080
ENV HOST=0.0.0.0

EXPOSE ${PORT}

WORKDIR /app
ADD . /app

RUN deno compile --allow-env --allow-read --allow-net --output dist/app --include src/db/migration/*.migration.ts src/index.ts

ENTRYPOINT ["dist/app"]

CMD ["run"]
